#!/usr/bin/python
# coding: utf-8


import datetime
import glob
import hashlib
import os
import string
import sys


# Template for index.html file
# project: gitlab project name
# pages_url: URL to base of pages, e.g., group.gitlab-pages.esrf.fr
# wheels_list: html snippet for the list of wheels
# date: date of the creation
index_template = string.Template("""<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>$project development snapshots</title>
</head>
<body>
<h1>$project development snapshots</h1>

<h2>Wheels and source</h2>
<p>To install a package, run:<br>
<code>pip install $project --pre --find-links https://$pages_url/$project/</code>
</p>
<h3>Available files:</h3>
<!-- List of .exe .dmg .whl and .tar.gz --!>
<p>
$wheels_list
</p>

$extra_h2

<h2>Debian/Ubuntu packages</h2>
<p>
It provides Debian/Ubuntu packages for amd64 computers for the following distributions:
</p>


$distributions

<h3>Information/Troubleshooting</h3>
<p>
<ul>
 <li>Packages are built automatically, hence not signed.
To install them, you have to accept the installation of non-signed packages. </li>
 <li>To change the priority of this repository, add a <code>/etc/apt/preferences.d/$project.pref</code> file containing:<br>
<pre><code>Package: *
Pin: origin "$pages_url/$project/"
Pin-Priority: 600</code></pre>
 </li>
</ul>
</p>

<p><small>Updated: $date</small></p>
</body>
</html>
""")


# template for a link to one wheel/tarball
wheel_template = string.Template("""
<li><a href="./$filename">$filename</a> - SHA256: <small><code>$sha256</code></small></li>\n
""")

# template for Windows application link
# filename: name of the file to download
windows_app_template = string.Template("""
    <h2>Windows</h2>
    <p>Prebuilt Windows application: <a href="./$filename">$filename</a></p>
    """)

# template for a debian/ubuntu distribution
# project: gitlab project name
# pages_url: URL to base of pages, e.g., group.gitlab-pages.esrf.fr
# distrib: Name of the Linux distribution
# distrib_capitalize: Same as distrib but with a leading capital letter
# apt_src_option: Additional flags to provide to apt source repo
distrib_template = string.Template("""
<h3>$distrib_capitalize</h3>
<p>
To use this repository (requires root access):
<ul>
    <li>Add a <code>/etc/apt/sources.list.d/$project.list</code> file containing:<br>
    <code>deb $apt_src_option https://$pages_url/$project/ $distrib main</code><br>
 </li>
 <li>Run: <code>apt-get update</code></li>
</ul>
</p>
<p><a href="./$distrib.txt">List of available packages</a></p>
""")

# Available apt source options: distribution name: option
apt_src_options = {
    'buster': '[allow-insecure=yes]',
    'focal': '[allow-insecure=yes]',
    }

# Create list of wheels and tarball
wheels_list = []
for filename in sorted(glob.glob("*.whl") + glob.glob("*.tar.gz") + glob.glob("*.exe") + glob.glob("*.dmg") + glob.glob("*.pkg")):
    hasher = hashlib.sha256()
    with open(filename, 'rb') as f:
        hasher.update(f.read())

    wheels_list.append(wheel_template.substitute(
        filename=filename,
        sha256=hasher.hexdigest()))

wheels_list = "<ul>\n" + "".join(wheels_list) + "</ul>\n"

project_name = sys.argv[1]

# Windows application if any
if "BOB_WINDOWS_APPLICATION" in os.environ:
    extra_h2 = windows_app_template.substitute(
        filename=os.environ["BOB_WINDOWS_APPLICATION"])
else:
    extra_h2 = ''

# Get available distributions by available .txt release files
distributions = [distrib_template.substitute(
    distrib=distrib[:-4],
    distrib_capitalize=distrib[:-4].capitalize(),
    apt_src_option=apt_src_options.get(distrib[:-4], ''),
    project=project_name,
    pages_url="silx.gitlab-pages.esrf.fr/bob",
    ) for distrib in glob.glob('*.txt')]


# Make final index page
index_content = index_template.substitute(
    project=project_name,
    pages_url="silx.gitlab-pages.esrf.fr/bob",
    wheels_list=wheels_list,
    extra_h2=extra_h2,
    distributions='\n'.join(distributions),
    date=datetime.datetime.now().strftime('%d-%m-%Y %H:%M'))
print(index_content)
