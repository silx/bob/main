--trusted-host www.silx.org
--find-links http://www.silx.org/pub/wheelhouse/
# Allow installation from source until wheels are available for python3.9
# --only-binary numpy

Cython
setuptools
build
wheel
mesonpep517
ninja
delocate; sys_platform == 'darwin'
numpy==1.12.0; python_version < '3.7'
numpy==1.14.5; python_version == '3.7'
numpy==1.17.3; python_version == '3.8' and (sys_platform != 'darwin' or platform_machine != 'arm64')
numpy==1.19.3; python_version == '3.9' and (sys_platform != 'darwin' or platform_machine != 'arm64')
numpy==1.21.0; python_version == '3.8' and sys_platform == 'darwin' and platform_machine == 'arm64'
numpy==1.21.0; python_version == '3.9' and sys_platform == 'darwin' and platform_machine == 'arm64'
numpy==1.21.3; python_version == '3.10'
numpy==1.23.2; python_version == '3.11'
numpy==1.26.0; python_version == '3.12'

