# bob: Builder Of Binary packages

This project provides common resources for automated build of Debian packages, Windows and manylinux wheels for different projects.

Python packages compilation information
=======================================

- [Packaging binary extensions](https://packaging.python.org/guides/packaging-binary-extensions/)

Windows
-------

- [Which Microsoft Visual C++ compiler to use?](https://wiki.python.org/moin/WindowsCompilers)
- Building extensions for Python 3.5: [Part 1](http://stevedower.id.au/blog/building-for-python-3-5/), [Part 2](http://stevedower.id.au/blog/building-for-python-3-5-part-two/)

macOS
-----

When building on macOS, the `MACOSX_DEPLOYMENT_TARGET` environment variable is taken into account
For maximum compatibility, it should be set to the target used by Python (i.e., `10.9` as of writing).

Linux
-----

- [manylinux dockers](https://github.com/pypa/manylinux)
